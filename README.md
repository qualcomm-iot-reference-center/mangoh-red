# mangOH Red - Customização de imagem com Yocto Project

A mangOH Red é uma placa de desenvolvimento da Sierra Wireless baseada em chipsets Qualcomm, com CPU de arquiteruta ARM.
O Chipset tem compatibilidade com o Kernel Linux, e pode rodar uma distribuição baseada no Yocto Project. Esse tutorial tem como objetivo explicar os passos básicos necessários para criar uma imagem customizada e gravá-la na placa.

### Observações gerais:
> Este tutorial foi escrito baseado nos módulos WP7603 e WP7702 da Sierra Wireless. Caso sua mangOH Red utilize um módulo diferente, podem ser necessários procedimentos diferentes.
> A máquina host utilizada tem sistema UBUNTU 18.04. O tutorial pode ser feito em distros diferentes (testado em Arch Linux), porém podem ser necessários passos adicionais para o funcionamento do Yocto e da ferramenta de gravação de imagem. Consulte os links do [Yocto](https://www.yoctoproject.org/docs/1.8/yocto-project-qs/yocto-project-qs.html) e da [ferramenta](https://source.sierrawireless.com/resources/airprime/software/swiflash/#sthash.gNFsd2Cp.dpbs) caso seja o seu caso.

## 1 - Conta da Sierra Wireless

O primeiro passo para qualquer desenvolvimento com a mangOH Red é criar uma conta, para obter acesso à documentação, códigos e firmwares disponibilizados pela empresa. Acesse o site [aqui](https://source.sierrawireless.com)

## 2 - Download dos arquivos necessários

<img src="images/sierra.png"
     alt="Markdown Monster icon"
     style="float: left; margin-right: 10px;" 
/>

Na aba Devices do site principal, selecione AirPrime, e na nova aba selecione WP series. Uma lista com os devices disponíveis vai ser exibida, selecione o correspontente ao seu módulo.

<img src="images/modulo.png"
     alt="Markdown Monster icon"
     style="float: left; margin-right: 10px;" 
/>

Na nova página, selecione a opção Firmware.

<img src="images/component.png"
     alt="Markdown Monster icon"
     style="float: left; margin-right: 10px;" 
/>

Na página de downloads, selecione a opção Component. Na imagem acima, o link está no link vermelho **WP76XX Firmware Release 13.1/13.2/13.3 Components**

<img src="images/source.png"
     alt="Markdown Monster icon"
     style="float: left; margin-right: 10px;" 
/>

O download que procuramos está na segunda tabela da nova página, **Rebuild Linux Distribution or/and Legato from source**. Escolha a versão do código fonte que desejar, no tutorial utilizamos a mais recente (SWI9X07Y_02.28.03.05 Source).

## 3 - Instalação e preparação

Depois de concluído o download, extraia o arquivo para a pasta que irá utilizar como workspace.
O primeiro passo a ser realizado é ativar o ambiente utilizado pelo Yocto para o trabalho da imagem. Navegue até a pasta extraída e rode o script de ativação:

```sh

cd yocto
source poky/oe-init-build-env build_bin

```

Esse comando irá criar uma pasta **build_bin** e ativar os scripts necessários, como o **bitbake**, utilizado para a compilação da imagem. Você também será direcionado para a pasta criada. Navegue até a pasta raiz para compilar a imagem padrão:

```sh

cd ..
make image_bin

```

Após o processo de compilação, será gerada uma imagem pronta para ser gravada na placa. Caso deseje fazer o teste, os arquivos são gravados no diretório tmp/deploy/images/<modelo>. Para gravar a imagem na placa consulte o tópico 6 deste guia.

## 4 - Customização da imagem

No Yocto Project, a customização é feita na alteração de arquivos, chamados de receitas, que são utilizados no processo de compilação. Os principais arquivos para começar o trabalho ficam no diretório **build_bin/conf**:

<img src="images/arquivos.png"
     alt="Markdown Monster icon"
     style="float: left; margin-right: 10px;" 
/>

O arquivo aberto nas imagem acima é o gerado durante os comandos executados no tópico 3.
Para adicionar um pacote na imagem, editamos o arquivo **local.conf**. Na foto a seguir, vamos adicionar o Python3, indicando a adição através da variável **CORE_IMAGE_EXTRA_INSTALL**:

<img src="images/localconf.png"
     alt="Markdown Monster icon"
     style="float: left; margin-right: 10px;" 
/>

Para que o pacote seja compilado corretamente, é necessário fazer o download da camada que contém o pacote e adicionar a receita ao projeto. O arquivo **bblayers.conf** contém a relação das receitas utilizadas para a compilação da imagem, e cada pacote que será compilado precisa ter a receita adicionada à relação. Observe que na imagem abaixo, a camada **meta-python** já está na relação presente em **bblayers.conf**:

<img src="images/bblayers.png"
     alt="Markdown Monster icon"
     style="float: left; margin-right: 10px;" 
/>

Como os dois arquivos já foram preenchidos corretamente, já é possível compilar a imagem, como detalhado no tópico 5. A seguir serão comentados mais alguns detalhes sobre adição de pacotes.

As camadas geralmente são mantidas em repositórios Git. Uma boa ferramenta para encontrar as camadas disponíveis é este [site](https://layers.openembedded.org), que reúne uma relação de camadas, receitas, classes, distros e máquinas disponiveis. A imagem a seguir ilustra a pesquisa que seria feita para adicionar o framework Tensorflow à uma imagem (apenas para fins ilustrativos, a mangOH Red não é o hardware mais adequado para rodar o framework). Primeiro, pesquisamos a receita (recipe) desejada:

<img src="images/recipe.png"
     alt="Markdown Monster icon"
     style="float: left; margin-right: 10px;" 
/>

Observando os resultados da busca, vemos que a camada (layer) meta-tensorflow contém a receita que procuramos. Na página da camada, podemos ver as dependências e o link do repositório Git:

<img src="images/layer.png"
     alt="Markdown Monster icon"
     style="float: left; margin-right: 10px;" 
/>

Com isso em mãos, o primeiro passo é fazer o clone do repositório fonte e de todas as dependências:

```

    git clone git://git.yoctoproject.org/meta-tensorflow
    git clone https://github.com/openembedded/meta-openembedded.git

```

Adicione o endereço completo das camadas ao arquivo bblayers.conf, como mostrado anteriormente. Adicione o nome das receitas a serem compiladas no arquivo local.conf.

##### Resumidamente, para adicionar pacotes na sua imagem customizada: Clone os repositórios necessários, adicione o caminho dos repositórios clonados ao arquivo bblayers.conf e adicione o nome do pacote à variável **CORE_IMAGE_EXTRA_INSTALL**, no arquivo **local.conf**.

## 5 - Compilação da imagem

Para compilar a imagem para a magOH Red, retorne até o diretório raiz do workspace (aquele que contém a pasta **poky**), e execute os comandos:

```sh

cd yocto
source poky/oe-init-build-env build_bin
bitbake -c compile -f linux-quic
bitbake -c build -f linux-quic
bitbake -c build -f mdm9x28-image-minimal

```

Após todo o processo de compilação, as imagens serão armazenadas no diretório **tmp/deploy/images/swi-mdm9x28-wp**. Para a produção deste tutorial, as imagens geradas ficaram com os nomes **yocto_wp76xx.4k.cwe** e **yocto_wp77xx.4k.cwe**.


## 6 - Gravação da imagem na placa

Para a gravação de imagens no Linux, a Sierra Wireless disponibiliza uma ferramenta, disponível neste [link](https://source.sierrawireless.com/resources/airprime/software/swiflash/#sthash.gNFsd2Cp.dpbs). Siga o tutorial disponibilizado no link para a instalação.

Conecte a mangOH Red ao PC com o cabo USB, e execute o comando:


```sh

swiflash -m "WP77XX" -i yocto_wp77xx.4k.cwe

```